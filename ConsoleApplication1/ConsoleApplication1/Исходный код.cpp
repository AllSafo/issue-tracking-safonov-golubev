#include <iostream>
#include <cstdlib>
#include <clocale>
#include <string>
#include <fstream>

using namespace std;

class User
{
public:
	char login[255], password[255];
};

void Login() // Авторизация
{
	system("cls");
	User userIn, readIn;
	cout << "Авторизация \n";
	cout << "Введите логин: ";
	cin >> userIn.login;
	cout << "Введите пароль: ";
	cin >> userIn.password;
	ifstream in("users.dat", ios_base::binary);
	bool flag = false;
	while (!in.eof())
	{
		in.read(reinterpret_cast<char *>(&readIn), sizeof(User));
		if (strcmp(userIn.login, readIn.login) == 0 && strcmp(userIn.password, readIn.password) == 0)
		{
			flag = true;
			break;
		}
	}
	in.close();

	if (flag)
	{
		cout << "Добро пожаловать в систему, " << userIn.login << "!\n";
	}
	else
	{
		cout << "Неправильный логин или пароль.\n";
	}
}


void Registr() // Регистрация. Нужна лишь для заполнения базы данных
{
	system("cls");
	User dataUser;
	ofstream out("users.dat", ios_base::binary | ios_base::app);
	cout << "Регистрация \n";
	cout << "Придумайте логин: ";
	cin >> dataUser.login;
	cout << "Придумайте пароль: ";
	cin >> dataUser.password;
	out.write(reinterpret_cast<char *>(&dataUser), sizeof(User));
	out.close();
	cout << "Спасибо за регистрацию, " << dataUser.login << "!" << endl;
}

void Menu() // меню
{
	cout << "Добро пожаловать!";
	cout << "Войти или зарегистрироваться? (y/n):  ";
	char temp;
	cin >> temp;
	if (temp == 'y')
	{
		Login();
	}
	else if (temp == 'n')
	{
		Registr();
	}
}

int main()
{
	setlocale(LC_ALL, "russian");
	Menu();
	system("pause");
	return 0;
}